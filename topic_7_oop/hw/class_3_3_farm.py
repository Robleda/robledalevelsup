from topic_7_oop.hw.class_3_1_chicken import Chicken
from topic_7_oop.hw.class_3_2_goat import Goat


class Farm:
    """
    Класс Farm.

    Поля:
        животные (list из произвольного количества Goat и Chicken): animals
                            (вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
        наименование фермы: name,
        имя владельца фермы: owner.

    Методы:
        get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
        get_chicken_count: вернуть количество куриц на ферме,
        get_animals_count: вернуть количество животных на ферме,
        get_milk_count: вернуть сколько молока можно получить в день,
        get_eggs_count: вернуть сколько яиц можно получить в день.
    """
    def __init__(self, name, owner):
        self.farm_animals = []
        self.name = name
        self.owner = owner

    def get_goat_count(self):
        pass

    def get_chicken_count(self):
        pass

    def get_animals_count(self):
        pass

    def get_milk_count(self):
        pass

    def get_eggs_count(self):
        pass


if __name__ == '__main__':
    farm = Farm('RobledaFarm', 'Const')
    chicken1 = Chicken('Петька', 1, 2)
    chicken2 = Chicken('Петух из Бременских музыкантов', 2, 3)
    goat1 = Goat('Дашка', 2, 4)
    goat2 = Goat('Прошка', 2, 5)