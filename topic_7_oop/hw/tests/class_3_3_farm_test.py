import pytest

from topic_7_oop.hw.class_3_1_chicken import Chicken
from topic_7_oop.hw.class_3_2_goat import Goat
from topic_7_oop.hw.class_3_3_farm import Farm


def test():
    name = 'Ферма'
    owner = 'Петя'

    farm = Farm(name, owner)

    assert farm.name == name
    assert farm.owner == owner

    assert farm.get_animals_count() == 0
    assert farm.get_chicken_count() == 0
    assert farm.get_goat_count() == 0
    assert farm.get_milk_count() == 0
    assert farm.get_eggs_count() == 0

    farm.animals.extend([
        Chicken(name='Хохлуша', corral_num=1, eggs_per_day=4),
        Chicken(name='Ряба', corral_num=1, eggs_per_day=6),
        Chicken(name='Краснушка', corral_num=2, eggs_per_day=5),

        Goat(name='Бешка', age=3, milk_per_day=20),
        Goat(name='Дереза', age=2, milk_per_day=15),
    ])

    assert farm.get_animals_count() == 5
    assert farm.get_chicken_count() == 3
    assert farm.get_goat_count() == 2
    assert farm.get_milk_count() == 35
    assert farm.get_eggs_count() == 15
