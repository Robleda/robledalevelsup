"""
Ввод данных с клавиатуры:
    - Имя
    - Фамилия
    - Возраст

Вывод данных в формате:
"Вас зовут <Имя> <Фамилия>! Ваш возраст равен <Возраст>."
"""

first_name = input('enter 1st name > ')
second_name = input('enter 2nd name > ')
age = input('enter yr age > ')

print(f'Вас зовут {first_name.title()} {second_name.title()}! Ваш возраст равен {age}.')
print('Вас зовут ' + first_name.title() + ' ' + second_name.title() + '! Ваш возраст равен ' + age + '.')
print('Вас зовут ', first_name.title(), ' ', second_name.title(), '! Ваш возраст равен ', age, '.', sep='')
