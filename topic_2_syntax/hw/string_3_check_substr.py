def check_substr(str_1: str, str_2: str):
    """
    Функция check_substr.

    Принимает две строки.
    Если меньшая по длине строка содержится в большей, то возвращает True,
    иначе False.
    Если строки равны, то False.
    Если одна из строк пустая, то True.
    """
    if len(str_1) < len(str_2):
        if str_1 in str_2:
            return True
        else:
            return False
    elif len(str_1) > len(str_2):
        if str_2 in str_1:
            return True
        else:
            return False
    elif str_1 == str_2:
        return False
