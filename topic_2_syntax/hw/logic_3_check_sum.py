def check_sum(digit_1: int, digit_2: int, digit_3: int):
    """
    Функция check_sum.

    Принимает 3 числа.
    Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
    """
    if digit_1 + digit_2 == digit_3 or digit_1 + digit_3 == digit_2 or digit_2 + digit_3 == digit_1:
        return True
    else:
        return False
