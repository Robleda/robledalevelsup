def print_symbols_if(my_str: str):
    """
    Функция print_symbols_if.

    Принимает строку.

    Если строка нулевой длины, то вывести строку "Empty string!".

    Если длина строки больше 5, то вывести первые три символа и последние три символа.
    Пример: string='123456789' => result='123789'

    Иначе вывести первый символ столько раз, какова длина строки.
    Пример: string='345' => result='333'
    """
    if len(my_str) == 0:
        return print('Empty string!')
    elif len(my_str) > 5:
        result = my_str[0:3] + my_str[-3:]
        return print(result)
    else:
        return print(my_str[0] * len(my_str))
