def arithmetic(digit_1: int, digit_2: int, operation: str):
    """
    Функция arithmetic.

    Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними.
    Если третий аргумент +, сложить их;
    если —, то вычесть;
    если *, то умножить;
    если /, то разделить (первое на второе).
    В остальных случаях вернуть строку "Unknown operator".
    Вернуть результат операции.
    """
    if operation == '+':
        return digit_1 + digit_2
    elif operation == '-':
        return digit_1 - digit_2
    elif operation == '*':
        return digit_1 * digit_2
    elif operation == '/':
        return digit_1 / digit_2
    else:
        return 'Unknown operator'
