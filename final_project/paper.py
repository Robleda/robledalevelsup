from final_project.goods import Goods


class Paper(Goods):
    """
    Штуки на чем пишут - бумага разная
    """
    goods_type = 'PaperDevice'

    def __init__(self, pap_format: int, density: int, name, price):
        super().__init__(name, price)

        # формат бумаги А1 - А16
        if pap_format < 1:
            self.pap_format = 1
        elif pap_format >= 16:
            self.pap_format = 16
        else:
            self.pap_format = pap_format

        # плотность бумаги 10 - 150 грамм на кв. метр
        if density < 10:
            self.density = 10
        elif density > 150:
            self.density = 150
        else:
            self.density = density

    def fold(self):
        return 'Бумага мнется и рвется, аккуратнее'

    def __str__(self):
        return f'{super().__str__()} | формат: А{self.pap_format} | плотность: {self.density} г/м2'


if __name__ == '__main__':
    pap1 = Paper(4, 22, 'test paper name', 111)
    print(pap1)
