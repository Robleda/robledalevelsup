from final_project.goods import Goods


class WritingSupplies(Goods):
    """
    Всякие штуки, которыми пишут
    """
    goods_type = 'WritingDevice'

    def __init__(self, material: str, length: int, name: str, price: int):
        super().__init__(name, price)

        # материал из чего сделано
        if material == '':
            self.material = 'обычный'
        else:
            self.material = material

        # длина 5 - 20 см
        if length < 5:
            self.length = 5
        elif length > 20:
            self.length = 20
        else:
            self.length = length

    def fold(self):
        return 'Принадлежности для письма можно согнуть или сломать'

    # не хочу нагромождать несущественные параметры, материал не будем выводить
    # def __str__(self):
    #     return f'{super().__str__()} | материал: {self.material}'


if __name__ == '__main__':
    test_wr_sup = WritingSupplies('пластм', 10, 'эрих краузе', 144)
    print(test_wr_sup)

