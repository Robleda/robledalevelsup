import pickle
import telebot
from telebot import types

from final_project.shop import Shop
from final_project.pen import Pen
from final_project.pencil import Pencil
from final_project.notebook import Notebook
from final_project.writing_paper import WritingPaper

bot = telebot.TeleBot('1042373825:AAGdCXHooY14SjXau2tb-NG8XNmTIpL6VlA')

shop_state = {}  # {message.from_user.id: [shop1, shop2, ... ]}
user_state = {}  # {message.from_user.id_1: Shop.title1, ... }
user_current_shop = {}  # {user_id1: current_shop, user_id2: None}

backup_file = 'bot_backup.pkl'

hide_board = types.ReplyKeyboardRemove()


@bot.message_handler(commands=['start'])
def help_handler(message):
    reply = f'RobledaBot welcomes you, {message.from_user.first_name}!\n' \
            f'Use known commands or type /help to learn more!'
    bot.send_message(message.chat.id, reply)


@bot.message_handler(commands=['help'])
def help_handler(message):
    reply = f'{message.from_user.first_name}, try commands below:\n' \
            f'Type /show_shops to display all "shops"\n' \
            f'Type /add_shop to add and edit your shop\n' \
            f'Type /stop and nothing happens.'
    bot.send_message(message.chat.id, reply)


def save_state():
    with open(backup_file, 'wb') as file:
        pickle.dump({'shop_state': shop_state, 'user_state': user_state, 'user_current_shop': user_current_shop}, file)


def load_state():
    global shop_state, user_state, user_current_shop

    try:
        with open(backup_file, 'rb') as file:
            bot_state = pickle.load(file)
            shop_state = bot_state.get('shop_state', {})
            user_state = bot_state.get('user_state', {})
            user_current_shop = bot_state.get('user_current_shop', {})
    except FileNotFoundError as error:
        print(f'File load error! {error}')
        shop_state = {}
        user_state = {}
        user_current_shop = {}


@bot.message_handler(commands=['show_shops'])
def show_shops(message):
    user_shops = shop_state.get(message.from_user.id, [])
    if len(user_shops) == 0:
        bot.send_message(message.chat.id, 'No shops! To add one use /add_shop command!')
    else:
        markup = types.InlineKeyboardMarkup()
        for index, shop in enumerate(user_shops):
            markup.add(types.InlineKeyboardButton(text=str(shop), callback_data=index + 1))

        bot.send_message(message.chat.id, 'Choose shop to browse', reply_markup=markup)


@bot.callback_query_handler(func=lambda call: not call.from_user.is_bot)  # call: True
def query_handler(call):
    global user_current_shop

    bot.answer_callback_query(callback_query_id=call.id, text='Info:')

    current_shop = shop_state[call.from_user.id][int(call.data) - 1]
    user_current_shop[call.from_user.id] = current_shop
    # # #
    save_state()
    # # #
    shop_info = current_shop.get_goods_info_str()

    y_n_kbd = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
    y_n_kbd.row('Yes!', 'No!')

    bot.send_message(call.message.chat.id, (shop_info if len(shop_info) > 0 else 'Late USSR (empty shelves)')
                     + '\n\nWant to add goods?', reply_markup=y_n_kbd)

    bot.register_next_step_handler(call.message, add_goods_from_ynkbd)


def add_goods_from_ynkbd(message):
    if message.text == 'Yes!':
        current_shop = user_current_shop.get(message.from_user.id)
        if current_shop is None:
            bot.send_message(message.from_user.id, 'No shop chosen! Enter /show_shops to', reply_markup=hide_board)
        else:
            goods_list_kbd = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
            goods_list_kbd.row('Pen', 'Pencil', 'Notebook', 'Writing paper')
            bot.send_message(message.chat.id, 'What stuff to add?', reply_markup=goods_list_kbd)
            bot.register_next_step_handler(message, choose_goods_type)
    else:
        bot.send_message(message.from_user.id, 'Okay', reply_markup=hide_board)


def choose_goods_type(message):
    if message.text == 'Pen':
        bot.send_message(message.chat.id,
                         'ink_type: str, core_diameter: float, material: str, length: int, name: str, price: int',
                         reply_markup=hide_board)

        bot.register_next_step_handler(message, add_pen)

    elif message.text == 'Pencil':
        bot.send_message(message.chat.id,
                         'color: str, core_diameter: float, material: str, length: int, name: str, price: int',
                         reply_markup=hide_board)

        bot.register_next_step_handler(message, add_pencil)

    elif message.text == 'Notebook':
        bot.send_message(message.chat.id,
                         'num_pages: int, cover_type: str, pap_format: int, density: int, name, price: int',
                         reply_markup=hide_board)

        bot.register_next_step_handler(message, add_notebook)

    elif message.text == 'Writing paper':
        bot.send_message(message.chat.id,
                         'stack_size: int, pap_format: int, density: int, name, price: int',
                         reply_markup=hide_board)

        bot.register_next_step_handler(message, add_writing_paper)


def add_pen(message):
    ink_type, core_diameter, material, length, name, price = message.text.split(', ')
    new_pen = Pen(ink_type, float(core_diameter), material, int(length), name, int(price))

    current_shop = user_current_shop.get(message.from_user.id)
    if current_shop is None:
        bot.send_message(message.from_user.id, 'No shop chosen! Enter /show_shops to')

    else:
        current_shop.add_goods(new_pen)
        bot.send_message(message.from_user.id, f'Added some:\n{new_pen}')
        # # #
        save_state()


def add_pencil(message):
    color, core_diameter, material, length, name, price = message.text.split(', ')
    new_pencil = Pencil(color, float(core_diameter), material, int(length), name, int(price))
    current_shop = user_current_shop.get(message.from_user.id)
    if current_shop is None:
        bot.send_message(message.from_user.id, 'No shop chosen! Enter /show_shops to')

    else:
        current_shop.add_goods(new_pencil)
        bot.send_message(message.from_user.id, f'Added some:\n{new_pencil}')
    # # #
    save_state()


def add_notebook(message):
    num_pages, cover_type, pap_format, density, name, price = message.text.split(', ')
    new_notebook = Notebook(int(num_pages), cover_type, int(pap_format), int(density), name, int(price))
    current_shop = user_current_shop.get(message.from_user.id)
    if current_shop is None:
        bot.send_message(message.from_user.id, 'No shop chosen! Enter /show_shops to')

    else:
        current_shop.add_goods(new_notebook)
        bot.send_message(message.from_user.id, f'Added some:\n{new_notebook}')
    # # #
    save_state()


def add_writing_paper(message):
    stack_size, pap_format, density, name, price = message.text.split(', ')
    new_wrpap = WritingPaper(int(stack_size), int(pap_format), int(density), name, int(price))
    current_shop = user_current_shop.get(message.from_user.id)
    if current_shop is None:
        bot.send_message(message.from_user.id, 'No shop chosen! Enter /show_shops to')

    else:
        current_shop.add_goods(new_wrpap)
        bot.send_message(message.from_user.id, f'Added some:\n{new_wrpap}')
    # # #
    save_state()


@bot.message_handler(commands=['add_shop'])
def add_shop(message):
    if user_state.get(message.from_user.id) is None:
        bot.send_message(message.chat.id, 'What is your name?')
        bot.register_next_step_handler(message, user_adds_shop)

    else:
        bot.send_message(message.chat.id, f'Hello, {message.from_user.first_name}!\n\nGive a name to your new shop:')
        bot.register_next_step_handler(message, user_adds_another_shop)
    # # #
    save_state()


def user_adds_shop(message):
    user_state[message.from_user.id] = message.from_user.first_name
    bot.send_message(message.chat.id, f'Nice to meet you, {message.text}!\n\nGive a name to your new shop:')
    bot.register_next_step_handler(message, user_adds_another_shop)
    # # #
    save_state()


def user_adds_another_shop(message):
    new_shop = Shop(message.text)
    if shop_state.get(message.from_user.id) is None:
        shop_state[message.from_user.id] = []
    shop_state[message.from_user.id].append(new_shop)

    bot.send_message(message.chat.id, f'Added shop:\n{new_shop}')
    # # #
    save_state()


if __name__ == '__main__':
    load_state()
    print('Loading...')
    bot.polling(none_stop=True, interval=0)
