class Goods:
    """
    Все товары
    """
    def __init__(self, name: str, price: int):
        self.name = name
        self.price = price

    def fold(self):
        return 'Товары можно испортить - помять, согнуть или сломать'

    def __str__(self):
        return f'"{self.name}" | цена: {self.price} рублей'


if __name__ == '__main__':
    test_goods = Goods('название', 100)
    print(test_goods)
