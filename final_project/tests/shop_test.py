import pytest

from final_project.goods import Goods
from final_project.pen import Pen
from final_project.pencil import Pencil
from final_project.notebook import Notebook
from final_project.writing_paper import WritingPaper
from final_project.shop import Shop


def test_init():
    shop_title = 'магазин'
    shop = Shop(shop_title)
    assert shop.shop_goods == dict()
    assert shop.shop_title == shop_title


def test_add_goods():
    pass


def test_str():
    pass


def test_get_goods_info_str():
    pass


def test_get_goods_by_category():
    pass


def test_get_goods_by_cat_str():
    pass


def test_get_type_of_most_big_goods_quantity():
    pass


def test_get_type_of_most_small_goods_quantity():
    pass


def test_add_goods_by_cat_list():
    pass


def test_add_goods_from_list():
    pass

