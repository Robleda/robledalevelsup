from final_project.writing_supplies import WritingSupplies


class Pen(WritingSupplies):
    """
    Ручка
    """
    def __init__(self, ink_type: str, core_diameter: float, material: str, length: int, name: str, price: int) -> object:
        super().__init__(material, length, name, price)

        # тип ручки - шариковая, гелевая, роллер итд
        if ink_type == '':
            self.ink_type = 'шариковая'
        else:
            self.ink_type = ink_type

        # толщина стержня (диаметр) 0.6 - 1.2 мм
        if core_diameter < 0.6:
            self.core_diameter = 0.6
        elif core_diameter > 1.2:
            self.core_diameter = 1.2
        else:
            self.core_diameter = core_diameter

    def fold(self):
        return 'Если сгибать ручку, то она сломается'

    def __str__(self):
        return f'Ручка {super().__str__()}  | тип чернил: {self.ink_type} | толщина стержня: {self.core_diameter} мм'


if __name__ == '__main__':
    test_pen = Pen('гель', 0.7, 'дерево', 7, 'parker', 200)
    print(test_pen)
