from final_project.paper import Paper


class WritingPaper(Paper):
    """
    Бумага для принтера в пачках
    """

    def __init__(self, stack_size: int, pap_format: int, density: int, name: str, price: int):
        super().__init__(pap_format, density, name, price)

        # кол-во листов в пачке кратно 100, но не более 1000
        if stack_size < 100:
            self.stack_size = 100
        elif stack_size > 1000:
            self.stack_size = 1000
        else:
            self.stack_size = (stack_size // 100) * 100

    def fold(self):
        return 'Листы бумаги легко рвутся'

    def __str__(self):
        return f'Бумага для принтера {super().__str__()}  | листов в пачке: {self.stack_size}'


if __name__ == '__main__':
    st1 = WritingPaper(220, 4, 80, 'svetocopy', 500)
    print(st1)
