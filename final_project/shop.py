import pickle

from final_project.pen import Pen
from final_project.pencil import Pencil
from final_project.notebook import Notebook
from final_project.writing_paper import WritingPaper


class Shop:
    def __init__(self, shop_title: str, should_load=False):
        self.shop_goods = {}

        # Получить/Изменить название Канцелярского магазина.
        self.shop_title = shop_title

        self.backup_file_name = f'{self.shop_title}.pkl'

        if should_load:
            self.load_state()
        else:
            self.shop_goods = {}

    def save_state(self):
        """
        Сохранялка в файл
        """
        with open(self.backup_file_name, 'wb') as file:
            pickle.dump(self.shop_goods, file)

    def load_state(self):
        """
        Загружалка из файла
        """
        try:
            with open(self.backup_file_name, 'rb') as file:
                self.shop_goods = pickle.load(file)
        except FileNotFoundError as error:
            self.shop_goods = {}
            print(f'ALARM!\n{error}')

    def add_goods(self, goods):
        goods_type = goods.goods_type
        if self.shop_goods.get(goods_type, None) is None:
            self.shop_goods[goods_type] = []

        self.shop_goods[goods_type].append(goods)

        self.save_state()

    def __str__(self):
        result_str = ''
        result_str += f'Магазинчик "{self.shop_title}"\n'
        result_str += self.get_goods_info_str()

        return result_str

    def get_goods_info_str(self):
        result_str = ''
        for goods_type, s_goods in self.shop_goods.items():
            result_str += f'{goods_type}:\n'
            for item in s_goods:
                result_str += f'\t{item}\n'

        return result_str

    def get_goods_by_category(self, category):
        return self.shop_goods.get(category, [])

    def get_goods_by_cat_str(self, category):
        goods_by_category = self.shop_goods.get(category, [])
        if not goods_by_category:
            return "Нет такой категории"
        else:
            result_str = f'Категория товаров: {category}\n'
            for goods in goods_by_category:
                result_str += f'\t{goods}\n'

            return result_str

    def get_type_of_most_big_goods_quantity(self):
        """
        Получить тип(ы) товаров с самым большим количеством в Канцелярском магазине.
        :return: Список из типов товаров
        """
        if len(self.shop_goods) == 0:
            return []

        goods_type_counter = {}
        for goods_type, goods in self.shop_goods.items():
            goods_type_counter[goods_type] = len(goods)

        max_counter = max(goods_type_counter.values())

        result_list = []
        for goods_type, counter in goods_type_counter.items():
            if counter == max_counter:
                result_list.append(goods_type)

        return result_list

    def get_type_of_most_small_goods_quantity(self):
        """
        Получить тип(ы) товаров с самым маленьким количеством в Канцелярском магазине.
        :return: Список из типов товаров
        """
        if len(self.shop_goods) == 0:
            return []

        goods_type_counter = {}
        for goods_type, goods in self.shop_goods.items():
            goods_type_counter[goods_type] = len(goods)

        min_counter = min(goods_type_counter.values())

        result_list = []
        for goods_type, counter in goods_type_counter.items():
            if counter == min_counter:
                result_list.append(goods_type)

        return result_list

    def add_goods_by_cat_list(self, category, goods_list):
        """
        Добавление товаров из списка с проверкой на категорию
        :param category: строка с типом, 'PaperDevice' или 'WritingDevice'
        :param goods_list: список с товарами
        :return: ничего, просто добавляет
        """
        try:
            for single_goods in goods_list:
                if single_goods.goods_type != category:
                    raise TypeError
                self.add_goods(single_goods)
        except TypeError:
            print('ERROR! Тип не тот!')

    def add_goods_from_list(self, goods_list):
        """
        Добавление товаров из списка
        :param goods_list: список с товарами
        :return: ничего, просто добавляет
        """
        for single_goods in goods_list:
            self.add_goods(single_goods)


if __name__ == '__main__':
    shop1 = Shop('Канцелярский рай и ад')
    pen1 = Pen('Гелевая', 1.1, 'пластик', 12, 'ЭрихКраузе', 101)
    pen2 = Pen('роллер', 0.6, 'пластилин', 22, 'автограф', 44)
    pencil1 = Pencil('радуга', 1.3, 'дерево', 4, 'праздничный карандаш', 25)
    pencil2 = Pencil('синий', 1.3, 'титан', 4, 'космический карандаш', 26)
    # shop1.add_goods(pen1)
    # shop1.add_goods(pen2)
    # shop1.add_goods(pencil1)
    shop1.add_goods(pencil2)

    wrpap1 = WritingPaper(220, 4, 80, 'copeerka', 500)
    nb1 = Notebook(40, '', 6, 40, 'Внешняя память', 72)
    shop1.add_goods(wrpap1)
    shop1.add_goods(nb1)

    print(shop1.get_goods_by_cat_str('WritingDevice'))
    print(shop1.get_goods_by_cat_str('PaperDevice'))

    print(shop1.get_type_of_most_big_goods_quantity())
    print(shop1.get_type_of_most_small_goods_quantity())

    print('---------------')
    shop1 = Shop('Канцелярский рай и ад')
    goods_list1 = [Pen('Гелевая', 1.1, 'пластик', 12, 'slayer', 101),
                   Pen('роллер', 0.6, 'пластилин', 22, 'anthrax', 44),
                   Pencil('радуга', 1.3, 'дерево', 4, 'napalm', 25),
                   Pencil('синий', 1.3, 'титан', 4, 'deth', 26),
                   WritingPaper(220, 4, 80, '54 метра', 500)]
    goods_list2 = [WritingPaper(220, 4, 80, '54 метра', 500),
                   Notebook(40, '', 6, 40, 'Внешняя память', 72),
                   WritingPaper(220, 4, 80, 'vodka', 500),
                   Notebook(40, '', 6, 40, 'debilnichek', 72)]
    # shop1.add_goods_by_cat_list('WritingDevice', goods_list1)
    # shop1.add_goods_from_list(goods_list2)
    print(shop1)
