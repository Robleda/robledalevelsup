from final_project.writing_supplies import WritingSupplies


class Pencil(WritingSupplies):
    """
    Карандаш
    """
    def __init__(self, color: str, core_diameter: float, material: str, length: int, name: str, price: int):
        super().__init__(material, length, name, price)

        # цвет карандаша
        if color == '':
            self.color = 'простой'
        else:
            self.color = color

        # толщина стержня (диаметр) 0.6 - 1.2 мм - как у ручки
        if core_diameter < 0.6:
            self.core_diameter = 0.6
        elif core_diameter > 1.2:
            self.core_diameter = 1.2
        else:
            self.core_diameter = core_diameter

    def fold(self):
        return 'Зеленые карандаши могут гнуться'

    def __str__(self):
        return f'Карандаш {super().__str__()}  | цвет: {self.color} | толщина стержня: {self.core_diameter} мм'
