from final_project.paper import Paper


class Notebook(Paper):
    """
    Блокнот
    """
    def __init__(self, num_pages: int, cover_type: str, pap_format: int, density: int, name: str, price: int):

        # кол-во страниц в блокноте 20 - 300
        super().__init__(pap_format, density, name, price)
        if num_pages < 20:
            self.num_pages = 20
        elif num_pages > 300:
            self.num_pages = 300
        else:
            self.num_pages = num_pages

        # тип обложки
        if cover_type == '':
            self.cover_type = 'Обычная обложка'
        else:
            self.cover_type = cover_type

    def fold(self):
        return 'Из блокнота вырываются странички'

    def __str__(self):
        return f'Блокнот {super().__str__()}  | страниц: {self.num_pages} | обложка: {self.cover_type}'


if __name__ == '__main__':
    nb1 = Notebook(40, '', 6, 40, 'Внешняя память', 72)
    print(nb1)
