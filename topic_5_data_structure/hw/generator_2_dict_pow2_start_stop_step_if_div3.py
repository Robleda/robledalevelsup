def dict_pow2_start_stop_step_if_div3(start: int, stop: int, step: int):
    """
    Функция dict_pow2_start_stop_step_if_div3.

    Принимает 3 аргумента: число start, stop, step.

    Возвращает генератор-выражение состоящий из кортежа (аналог dict):
    (значение, значение в квадрате)
    при этом значения перебираются от start до stop (не включая) с шагом step
    только для чисел, которые делятся на 3 без остатка.

    Пример: start=0, stop=10, step=1 результат ((0, 0), (3, 9), (6, 36), (9, 81)).

    Если start или stop или step не являются int, то вернуть строку 'Start and Stop and Step must be int!'.
    Если step равен 0, то вернуть строку "Step can't be zero!"
    """
    if type(start) != int or type(stop) != int or type(step) != int:
        return 'Start and Stop and Step must be int!'
    elif step == 0:
        return "Step can't be zero!"

    return ((i, i ** 2) for i in range(start, stop, step) if i % 3 == 0)


if __name__ == '__main__':
    a = dict_pow2_start_stop_step_if_div3(-9, 9, 1)
    for i in range(9):
        print(next(a))
