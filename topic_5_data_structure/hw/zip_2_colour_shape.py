def zip_colour_shape(colors_list: list, shapes_tuple: tuple):
    """
    Функция zip_colour_shape.

    Принимает 2 аргумента: список с цветами(зеленый, красный) и кортеж с формами (квадрат, круг).

    Возвращает список с парами значений из каждого аргумента.

    Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
    Если вместо tuple передано что-то другое, то возвращать строку 'Second arg must be tuple!'.
    Но если вместо tuple передана str, то конвертируем str -> tuple.

    Если list пуст, то возвращать строку 'Empty list!'.
    Если tuple пуст, то возвращать строку 'Empty tuple!'.

    Если list и set различного размера, обрезаем до минимального (стандартный zip).
    """
    if type(colors_list) != list:
        return 'First arg must be list!'

    if type(shapes_tuple) != tuple:
        return 'Second arg must be tuple!'
    elif len(colors_list) == 0:
        return 'Empty list!'

    if type(shapes_tuple) == str:
        pass
    elif len(shapes_tuple) == 0:
        return 'Empty tuple!'

    return list(zip(colors_list, shapes_tuple))
