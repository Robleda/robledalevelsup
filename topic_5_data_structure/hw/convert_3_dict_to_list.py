def dict_to_list(my_dict: dict):
    """
    Функция dict_to_list.

    Принимает 1 аргумент: словарь.

    Возвращает (список ключей,
                список значений,
                количество уникальных элементов в списке ключей,
                количество уникальных элементов в списке значений).

    Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.
    """
    if type(my_dict) != dict:
        return 'Must be dict!'

    keys = []
    vals = []
    for k, v in my_dict.items():
        keys.append(k)
        vals.append(v)
    return keys, vals, len(set(keys)), len(set(vals))
