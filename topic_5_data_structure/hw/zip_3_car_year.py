from itertools import zip_longest


def zip_car_year(car_list: list, year_list: list):
    """
    Функция zip_car_year.

    Принимает 2 аргумента: список с машинами и список с годами производства.

    Возвращает список с парами значений из каждого аргумента, если один список больше другого,
    то заполнить недостающие элементы строкой “???”.

    Подсказка: zip_longest.

    Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
    Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
    """
    if type(car_list) != list or type(year_list) != list:
        return 'Must be list!'
    elif len(car_list) == 0 or len(year_list) == 0:
        return 'Empty list!'

    return list(zip_longest(car_list, year_list, fillvalue='???'))
