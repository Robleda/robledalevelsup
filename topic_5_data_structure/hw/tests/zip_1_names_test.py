import pytest

from topic_5_data_structure.hw.zip_1_names import zip_names

params = [
    (None, None, 'First arg must be list!'),
    (0, (1, 2, 3), 'First arg must be list!'),
    ('0', (1, 2, 3), 'First arg must be list!'),

    ([], None, 'Second arg must be set!'),
    ([], 0, 'Second arg must be set!'),
    ([], '0', 'Second arg must be set!'),


    ([], set(), 'Empty list!'),
    ([1], set(), 'Empty set!'),

    (['Rose'], {'Black'}, [('Rose', 'Black')]),
    (['Rose'], {'Black', 'Pink'}, [('Rose', 'Black')]),
    (['Rose', 'Mark'], {'Black', 'Pink'}, [('Rose', 'Pink'), ('Mark', 'Black')]),
]

ids = ["name_list: %s | surname_set: %s => %s" % (name_list, surname_set, expected) for (name_list, surname_set, expected) in params]


@pytest.mark.parametrize(argnames="name_list, surname_set, expected",
                         argvalues=params,
                         ids=ids)
def test_zip_names(name_list, surname_set, expected):
    if type(expected) == str:
        assert zip_names(name_list, surname_set) == expected
    else:
        result = zip_names(name_list, surname_set)
        assert len(result) == len(expected)
        # TODO: более точная проверка set (может менять порядок)


