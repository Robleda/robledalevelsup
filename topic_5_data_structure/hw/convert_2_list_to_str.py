def list_to_str(my_list: list, sep: str):
    """
    Функция list_to_str.

    Принимает 2 аргумента: список и разделитель (строка).

    Возвращает (строку полученную разделением элементов списка разделителем,
                количество разделителей в получившейся строке в квадрате).

    Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

    Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.

    Если список пуст, то возвращать пустой tuple().

    ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
    """
    if type(my_list) != list:
        return 'First arg must be list!'
    elif len(my_list) == 0:
        return ()

    if type(sep) != str:
        return 'Second arg must be str!'

    my_str = sep.join([str(elem) for elem in my_list])
    count = 0
    for i in my_str:
        if i == sep:
            count += 1

    return my_str, count ** 2
